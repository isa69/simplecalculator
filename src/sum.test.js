const sum = require("./sum.js");

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
  });
  
  test('add 1+é equal 3 but with strings', () => {
    expect(sum('1', '2')).toBe(3);
});